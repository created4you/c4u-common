<?php

if (!function_exists('prx')) {
	function prx($value) {
		echo '<pre>';
		print_r($value);
		exit();
	}
}

if (!function_exists('pr')) {
	function pr($value) {
		echo '<pre>';
		print_r($value);
		echo '</pre>';
	}
}

if (!function_exists('vdx')) {
	function vdx($value) {
		echo '<pre>';
		var_dump($value);
		exit();
	}
}

if (!function_exists('vd')) {
	function vd($value) {
		echo '<pre>';
		var_dump($value);
		echo '</pre>';
	}
}
