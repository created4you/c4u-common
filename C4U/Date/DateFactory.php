<?php

namespace C4U\Date;

class DateFactory {

	public static function now() {
		return new Date(time());
	}

	public static function fromUnixTime($value) {
		return new Date((int)$value);
	}

	public static function fromFormat($format, $value) {
		$value = \DateTime::createFromFormat($format, $value);
		if ($value) {
			return new Date($value->getTimestamp());
		}
		return new Date();
	}

	public static function fromSqlDate($value) {
		if (self::checkSqlDate($value)) {
			$value = preg_replace('~^(\d+)-(\d+)-(\d+).*$~', '\3.\2.\1', $value);
			$value = \DateTime::createFromFormat('d.m.Y', $value);
			if ($value) {
				return new Date($value->getTimestamp());
			}
		}
		return new Date();
	}

	public static function fromSqlDateTime($value) {
		if (self::checkSqlDate($value)) {
			$value = preg_replace('~^(\d+)-(\d+)-(\d+) ([0-9]{2}):([0-9]{2}):([0-9]{2})~', '\3.\2.\1 \4:\5:\6' , $value);
			$value = \DateTime::createFromFormat('d.m.Y G:i:s', $value);
			if ($value) {
				return new Date($value->getTimestamp());
			}
		}
		return new Date();
	}

	public static function fromDatePicker($value) {
		$value = \DateTime::createFromFormat('d.m.Y', $value);
		if ($value) {
			return new Date($value->getTimestamp());
		}
		return new Date();
	}

	public static function fromDatePickerWithTime($value) {
		$value = \DateTime::createFromFormat('d.m.Y G:i', $value);
		if ($value) {
			return new Date($value->getTimestamp());
		}
		return new Date();
	}

	public static function fromCzechDate($value) {
		$value = \DateTime::createFromFormat('d.m.Y', $value);
		if ($value) {
			return new Date($value->getTimestamp());
		}
		return new Date();
	}

	public static function fromUsDate($value) {
		$value = \DateTime::createFromFormat('m/d/Y', $value);
		if ($value) {
			return new Date($value->getTimestamp());
		}
		return new Date();
	}

	public static function fromDateTime(\DateTime $value) {
		return new Date($value->getTimestamp());
	}

	private static function checkSqlDate($value) {
		if (!$value || $value == '0000-00-00' || $value == '1970-01-01 00:00:00' || $value == '1970-01-01') {
			return false;
		}
		return true;
	}

	public static function fromISO8601($value) {
		// Fixme.
		return self::fromSqlDateTime(substr($value, 0, 10) . ' ' . substr($value, 11, 8)) ;
	}

}