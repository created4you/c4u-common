<?php

namespace C4U\Date\DateCalculator;

use C4U\Date\Date;
use C4U\Date\DateFactory;
use C4U\Date\DayName;

class DaysCalculator {

	public function getDateRange(Date $dateFrom, Date $dateTo) {
		return $this->getRange($dateFrom, $dateTo, new \DateInterval("P1D"));
	}

	private function getRange(Date $dateFrom, Date $dateTo, \DateInterval $dateInterval) {
		$output = array();
		$actual = $dateFrom->toDateTime();
		for ($i = 0; $i < $this->getDaysDiff($dateFrom, $dateTo); $i++) {
			$output[] = DateFactory::fromDateTime($actual);
			$actual = $actual->add($dateInterval);
		}
		return $output;
	}

	public function getDaysDiff(Date $dateFrom, Date $dateTo) {
		$diff = $dateFrom->toDateTime()->diff($dateTo->toDateTime());
		if ($diff) {
			return $diff->days;
		}
		return 0;
	}

	public function getEarlier(Date $first, Date $second) {
		if ($first->toUnixTime() == "") return $second;
		if ($second->toUnixTime() == "") return $first;
		return $first->toDateTime() > $second->toDateTime() ? $first : $second;
	}

	public function getLater(Date $first, Date $second) {
		if ($first->toUnixTime() == "") return $second;
		if ($second->toUnixTime() == "") return $first;
		return $first->toDateTime() < $second->toDateTime() ? $first : $second;
	}

	public function firstWeekdayAfterDate($dayNumber, Date $startDate, Date $endDate) {
		if ($this->getWeekdayNumber($startDate) == $dayNumber) {
			return $startDate;
		}
		$firstDay = strtotime('next ' . DayName::getDayName($dayNumber, 'en') ,$startDate->toUnixTime());
		if ($firstDay > $endDate->toUnixTime()) {
			return null;
		}
		return DateFactory::fromUnixTime($firstDay);
	}

	public function getWeekdayNumber(Date $date) {
		$day = date('w', $date->toUnixTime());
		if ($day == 0) return 7;
		return $day;
	}

	public function getWeekdaysRange(Date $startDate, Date $endDate) {
		$actualDate = $startDate->toUnixTime();
		$output = array();
		if ($actualDate >  $endDate->toUnixTime()) {
			return $output;
		}
		while ($actualDate <= $endDate->toUnixTime()) {
			$output[] = DateFactory::fromUnixTime($actualDate);
			$actualDate = strtotime('+1 week', $actualDate);
		}
		return $output;
	}
}