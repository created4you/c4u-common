<?php

namespace C4U\Date;

class DayName {

	public static $dayNames = array(
		'Pondělí',
		'Úterý',
		'Středa',
		'Čtvrtek',
		'Pátek',
		'Sobota',
		'Neděle',
	);

	public static $dayNamesEn = array(
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday',
	);

	public static $shortNames = array(
		'Po',
		'Út',
		'St',
		'Čt',
		'Pá',
		'So',
		'Ne',
	);

	public static function getDayName($dayNo, $lang = 'cz') {
		$dayNo--;
		if ($lang == 'en') {
			return isset(self::$dayNamesEn[$dayNo]) ? self::$dayNamesEn[$dayNo] : null;
		} else {
			return isset(self::$dayNames[$dayNo]) ? self::$dayNames[$dayNo] : null;
		}
	}

	public static function getShortDayName($dayNo) {
		$dayNo--;
		return isset(self::$shortNames[$dayNo]) ? self::$shortNames[$dayNo] : null;
	}
}