<?php

namespace C4U\Date;

class DateUtils {

	public function datepicker2Unixtime($datepickerTime, $withTime = false) {
		if ($withTime) {
			$date = \DateTime::createFromFormat('d.m.Y G:i', $datepickerTime);
		} else {
			$date = \DateTime::createFromFormat('d.m.Y', $datepickerTime);
		}
		if ($date) {
			return $date->getTimestamp();
		}
		return null;
	}

	public function datetime2Unixtime($datetime) {
		$date = \DateTime::createFromFormat('Y-m-d G:i:s', $datetime);
		if ($date) {
			return $date->getTimestamp();
		}
		return null;
	}

	public function unixTime2datetime($unixTime = null) {
		return strftime('%Y-%m-%d %H:%M:%S', $unixTime);
	}

	public function datetime2niceTime($datetime) {
		return strftime('%d.%m.%Y %H:%M', $this->datetime2Unixtime($datetime));
	}

	public function parseDatetime($datetime, $outputType = 'date') {
		if (!$datetime || $datetime == '0000-00-00' || $datetime == '1970-01-01 00:00:00') {
			return null;
		}
		if ($outputType == 'date') {
			return preg_replace('~^(\d+)-(\d+)-(\d+).*$~', '\3.\2.\1', $datetime);
		} else if ($outputType == 'time') {
			if (preg_match('~^.+? ([0-9]{2}:[0-9]{2}):([0-9]{2})~', $datetime, $matches)) {
				return $matches[1];
			}
			return $datetime;
		}
	}

	public function addWeeks($now, $howLong) {
		return strtotime('+ ' . $howLong . ' weeks', $now);
	}

	public function sec2min($seconds) {
		return sprintf('%02s:%02s', (int)($seconds / 60), $seconds % 60);
	}

	public function datePicker2dateTime($datePicker, $useTime = false) {
		if ($useTime) {
			$datePicker .= ' 00:00';
		}
		$unixTime = $this->datepicker2Unixtime($datePicker, $useTime);
		return $this->unixTime2datetime($unixTime);
	}

	public function dateDiff($from, $to) {
		$from = new \DateTime($from);
		$to = new \DateTime($to);
		return $from->diff($to);
	}
}
