<?php

namespace C4U\Date\DateCalculator;

use C4U\Date\Date;
use C4U\Date\DateFactory;

class DateCalculator {

	public function getDaysRange(Date $dateFrom, Date $dateTo, $days) {
		$days = explode(';', $days);
		$output = array();
		if (count($days)) {
			foreach ($days as $day) {
				$output[] = $this->getDayOccurrenceRange($dateFrom, $dateTo, $day);
			}
		}
		return $output;
	}

	private function getDayOccurrenceRange(Date $dateFrom, Date $dateTo, $day) {

	}

	public function getRange(Date $dateFrom, Date $dateTo, \DateInterval $dateInterval) {
		$output = array();
		$actual = $dateFrom->toDateTime();
		for ($i = 0; $i < $this->getDaysDiff($dateFrom, $dateTo); $i++) {
			$output[] = DateFactory::fromDateTime($actual);
			$actual = $actual->add($dateInterval);
			if ($actual->getTimestamp() > $dateTo->toUnixTime()) {
				break;
			}
		}
		return $output;
	}

	public function getDaysDiff(Date $dateFrom, Date $dateTo) {
		$diff = $dateFrom->toDateTime()->diff($dateTo->toDateTime());

		if ($diff) {
			return $diff->days + 1;
		}
		return 0;
	}

	public static function translateDiff(Date $first, Date $second = null) {
		if (!$second) $second = DateFactory::now()->toDateTime();
		$first = $first->toDateTime();
		/** @var \DateInterval $diff */
		$diff = $second->diff($first);
		if ($diff->d > 0) return $diff->format('%dd %hh %im');
		if ($diff->d == 0 && $diff->h > 0) return $diff->format('%hh %im');
		return $diff->format('%im');
	}

	public function getEarlier(Date $first, Date $second) {
		if ($first->toUnixTime() == "") return $second;
		if ($second->toUnixTime() == "") return $first;
		return $first->toDateTime() > $second->toDateTime() ? $first : $second;
	}

	public function getLater(Date $first, Date $second) {
		if ($first->toUnixTime() == "") return $second;
		if ($second->toUnixTime() == "") return $first;
		return $first->toDateTime() < $second->toDateTime() ? $first : $second;
	}

	public function getTotal(Date $startOfYear, Date $endOfYear, $total, $excludedDays) {
		$output = array();

		if (!$total || $total == 0) return $output;

		$interval = floor((date('z', $endOfYear->toUnixTime()) + 1) / $total);
		$actualDate = $startOfYear;

		for ($i = 0; $i < $total; $i++) {
			if ($this->isExcluded($actualDate, $excludedDays)) {
				$actualDate = DateFactory::fromUnixTime(strtotime('+1 day', $actualDate->toUnixTime()));
				$i--;
			} else {
				$output[] = $actualDate;
				$actualDate = DateFactory::fromUnixTime(strtotime('+' . $interval . ' days', $actualDate->toUnixTime()));
			}
		}

		return $output;
	}

	private function isExcluded(Date $actualDate, array $excludedDays) {
		foreach ($excludedDays as $excludedDay) {
			if ($excludedDay->toCzechDate() == $actualDate->toCzechDate()) return true;
		}
		return false;
	}

	public function getWeekStart($weekNo, $year) {
		$date = new \DateTime();
		$date->setISODate($year, $weekNo);
		return DateFactory::fromCzechDate($date->format('d.m.Y'));
	}

	public function getWeekEnd($weekNo, $year) {
		$date = new \DateTime();
		$date->setISODate($year, $weekNo);
		$date->modify('+6 days');
		return DateFactory::fromCzechDate($date->format('d.m.Y'));
	}

}