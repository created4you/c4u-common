<?php

namespace C4U\Date;

class WeekUtils {

	public static function generateWeeksArray($year, $withTitles = false, $encodeYear = false) {
		$startDate = DateFactory::fromCzechDate('1.1.' . $year);
		$weekStart = DateFactory::fromUnixTime(strtotime('last monday', $startDate->toUnixTime()));
		$output = array();
		for ($weekNo = 1; $weekNo <= 52; $weekNo++) {
			$key = $weekNo . ($encodeYear ? '_' . $year : '');
			$weekEnd = DateFactory::fromUnixTime(strtotime('+6 days', $weekStart->toUnixTime()));
			$output[$key] = $weekNo . ($withTitles ? ' (' . $weekStart->toCzechDate() . ' - ' . $weekEnd->toCzechDate() . ')' : '');
			$weekStart = DateFactory::fromUnixTime(strtotime('+7 days', $weekStart->toUnixTime()));
		}
		return $output;
	}

	public static function getWeekRangeFromWeekNo($weekNo, $year) {
		$dto = new \DateTime();
		$output = array();
		$output['start'] = DateFactory::fromDateTime($dto->setISODate($year, $weekNo));
		$output['end'] = DateFactory::fromDateTime($dto->modify('+6 days'));
		return $output;
	}

	public static function formatWeekRangeFromWeekNo($weekNo, $year) {
		$data = self::getWeekRangeFromWeekNo($weekNo, $year);
		if (!$data || !is_array($data) || count($data) < 2) return "";
		$startDate = $data['start'] instanceof Date ? $data['start']->toCzechDate() : null;
		$endDate = $data['end'] instanceof Date ? $data['end']->toCzechDate() : null;

		if ($startDate && $endDate) {
			return $startDate . ' - ' . $endDate;
		}

		return '';
	}

}
