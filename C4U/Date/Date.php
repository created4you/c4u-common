<?php

namespace C4U\Date;

class Date {

	/* UnixTime */
	private $value = "";

	public function __construct($value = null) {
		$this->value = $value;
	}

	public function toUnixTime() {
		return $this->value;
	}

	public function toDateTime() {
		$dateTime = new \DateTime();
		if (is_long($this->value)) {
			return $dateTime->setTimestamp($this->value);
		}
		return $dateTime;
	}

	public function toCzechDate() {
		if (is_long($this->value)) {
			return date('d.m.Y', $this->value);
		}
		return null;
	}

	public function toUsDate() {
		if (is_long($this->value)) {
			return date('m/d/Y', $this->value);
		}
		return null;
	}

	public function toSqlDate() {
		if (is_long($this->value)) {
			return date('Y-m-d', $this->value);
		}
		return null;
	}

	public function toSqlDateTime() {
		if (is_long($this->value)) {
			return date('Y-m-d H:i:s', $this->value);
		}
		return null;
	}

	public function toCzechDateTime() {
		if (is_long($this->value)) {
			return date('d.m.Y H:i:s', $this->value);
		}
		return null;
	}

	public function format($string) {
		if (is_long($this->value)) {
			return date($string, $this->value);
		}
		return null;
	}

	public function isPresent() {
		return !!$this->value;
	}

}